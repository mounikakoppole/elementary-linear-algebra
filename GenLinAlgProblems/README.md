# GenLinAlgProblems

[![Build Status](https://gitlab.com/ea42gh/GenLinAlgProblems.jl/actions/workflows/CI.yml/badge.svg?branch=main)](https://github.com/ea42gh/GenLinAlgProblems.jl/actions/workflows/CI.yml?query=branch%3Amain)
[![Coverage](https://codecov.io/gh/ea42gh/GenLinAlgProblems.jl/branch/main/graph/badge.svg)](https://codecov.io/gh/ea42gh/GenLinAlgProblems.jl)

Generate Linear Algebra Problems requiring simple arithmetic.
Problem solutions can be generated in the browser using itikz with nicematrix

Problems Types Covered
* Gaussian Elimnation Problems
* QR problems
* EigenProblems, including the SVD
