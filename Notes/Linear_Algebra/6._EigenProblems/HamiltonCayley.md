
---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'HamiltonCayley.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. The Hamilton Cayley Theorm and the Inverse of a Matrix
## 1.1 Preliminary
## 1.2 Example
## 1.3 The Cayley Hamilton Theorem
## 1.4 Application: a Formula for the Inverse
### 1.4.1 General 2x2 matrix
### 1.4.2 Continuation of the 3x3 Example
