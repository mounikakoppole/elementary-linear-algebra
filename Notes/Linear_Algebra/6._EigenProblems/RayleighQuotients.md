---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'RayleighQuotients.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. The Normalized Quadratic Form
## 1.1 Power Iteration
## 1.2. The Reduced SVD $\mathbf{A = U_r \Sigma_r V_r^t}$
# 2. Rayleigh Quotient Definition and Theorem
## 2.1 Definition
## 2.2. A 2x2 Example, Symmetric Matrix
## 2.3. A 2x2 Example, Non-Symmetric Matrix
## 2.4 A 3x3 Example
# 3. Minimax Theorem
