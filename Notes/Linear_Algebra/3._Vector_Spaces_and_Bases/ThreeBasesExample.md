---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'ThreeBasesExample.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Data
# 2. Columns of the Identity Matrix
# 3. Fourier Basis (Sines and Cosines)
# 4. Haar Wavelet Basis
# 5. Take Away
