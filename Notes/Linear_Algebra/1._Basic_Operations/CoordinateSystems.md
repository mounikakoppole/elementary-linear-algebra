---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'CoordinateSystems.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Substituting Linear Expressions: Matrix Multiplication
## 1.1 A simple Example
## 1.2. The General Case
# 2. The Column View of A x = b
## 2.1 Decompose a Vector
## 2.2 Example Computation
## 2.3 Conclusion
# 3. Changing Coordinate Systems
## 3.1 Just another Coordinate System
## 3.2 Relation between Coordinate Vectors
# 4. Suggestion
