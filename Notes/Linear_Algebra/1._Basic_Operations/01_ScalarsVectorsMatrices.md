---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '01_ScalarsVectorsMatrices.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Introduction
## 1.1 Scalars
## 1.2 Vectors and Matrices
# 2. Special Cases
## 2.1 Matrices With a Single Row or a Single Column
## 2.2 Zero Matrix, Identity Matrix
# 3. Geometrical Representation of Vectors
## 3.1 Arrows
## 3.2 Functions
# 4. Take Away
