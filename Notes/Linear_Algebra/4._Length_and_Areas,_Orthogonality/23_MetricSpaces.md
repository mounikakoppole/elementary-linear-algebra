---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '23_MetricSpaces.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Inner Products
## 1.1 Inner Products and Metrics in $\mathbb{R}^n$
## 1.2  Inner Products in Function Spaces
# 2. Orthogonality
# 3. Gram Schmidt
# 4. Take Away
