---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'LeastMeanSquares.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Perform an experiment
# 2. Conjecture a linear model
## 2.1 Solve the resulting Normal Equations
### 2.1.1 Compute and solve the normal equation:
### 2.1.2 Compute the projections
# 3. Conjecture a cubic model
# 4. Overfitting and Ringing
# 5. Exercises
