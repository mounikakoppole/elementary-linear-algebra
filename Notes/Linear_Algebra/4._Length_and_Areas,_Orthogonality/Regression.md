---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'Regression.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Create some data
## 1.1 Pandas DataFrame
# 1.2 Take a look at the data
# 2. Let's fit some model
## 2.1 Let's try a line:  y = a + b x
### 2.1.1 Solve the normal equation for $x$
