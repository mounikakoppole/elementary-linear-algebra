---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '20_LengthOrthogonality.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Adding Vector Length to Vector Spaces
## 1.1 Basic Definitions
## 1.2 Inequalities, Angle, Orthogonal Vectors
# 2. Fundamental Theorem of Linear Algebra (Part 2)
## 2.1 Main Definitions and Theorem
### 2.1.1 Linear Independence of Orthogonal Vectors
### 2.1.2 Mutually Orthogonal Vectors
### 2.1.3 Orthogonal Spaces
## 2.2 Use the Fundamental Theorem to Decompose a Vector (Naive Method)
## 2.3 Use the Fundamental Theorem to Decompose a Vector (Refinement)
### 2.3.1 Key Observation 1: Decomposing a Vector into Orthogonal Components
### 2.3.2 Key Observation 2: No Need to Identify the Column Space of $A$ and the Null Space of $A^t$
### 2.3.3 The Final Touch: Rewrite the Equations in Matrix Form
# 3. The Normal Equation
## 3.1 Basic Properties of the Normal Equation
### 3.1.1 The Normal Equation
### 3.1.2 The Equivalent Minimization Problem
### 3.1.3 Example: Projection Onto a Hyperplane
## 3.2 Special Case: Projection onto a Line
## 3.3 Special Case: the Columns of $A$ are Mutually Orthogonal
# 4. Take Away
## 4.1 The Fundamental Theorem of Linear Algebra 
## 4.2 The Normal Equation
