---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'QR_orthogonal_polynomials.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Gram Schmidt Procedure
# 2. Convert Power Series to Legendre Polynomials Expansion
# 3. Fit a Function
