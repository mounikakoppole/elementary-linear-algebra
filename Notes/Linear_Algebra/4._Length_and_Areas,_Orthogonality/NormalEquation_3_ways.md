---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'NormalEquation.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 0. Generate Some Data
# 1. Gaussian Elimination  applied to $\mathbf{\left( A^t A \mid A^t b \right)}$
# 2. Using the $\mathbf{QR}$ Decomposition
# 3. Using the SVD
