---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'MeanAndStdProjections.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Mean and Deviation from the Mean
## 1.1 The Mean of a Set of Samples
## 1.2 The Deviation from the Mean
# 2 Sandard Deviation, Covariance and Correlation
## 2.1 Standard Deviation
## 2.2 Covariance and Correlation
## 2.3 Covariance and Correlation Matrices
