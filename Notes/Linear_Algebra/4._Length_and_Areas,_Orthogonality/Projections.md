---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'Projections.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Dual Basis
# 2. Oblique Projections
# 3. Orthogonal Projections
# 4. Diagonalizable Matrices
# 5. Application Examples
## 5.2 A Projection Problem (Spring 14)
