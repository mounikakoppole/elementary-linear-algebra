
---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '10d_LinearTx_Examples.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Coordinate Vector, Change of Coordinates
## 1.1 $\mathbb{R}^2 \longrightarrow \mathbb{R}^2$ Example
## 1.2 $\mathscr{P}_2\left[ -1,1\right] \longrightarrow \mathbb{R}^3$ Example
# 2. Linear Transformations Can be Combined, Resulting in Linear Transformations
## 2.1 Let's Combine Two Linear Transformations
### 2.1.1 Two Linear Transformations from $\mathbf{\mathbb{R}^n}$ to $\mathbf{\mathbb{R}^m}$
### 2.1.2 Polynomials, Coordinate Vectors and Linear Transforms
## 2.2 Combine these Ideas
## 2.3 Conclusion
# 3. Givens Rotations, Householder Reflections
