
---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'NonLinearTransformations.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Effect of Linear and NonLinear Transformations
## 1.1 Random Linear Transform
## 1.2 Non-linear Transform of the Data
# 2. Suggestion
