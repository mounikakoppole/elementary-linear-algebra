---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '12_LU_decomposition.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Basic Idea
## 1.1 Multiple Right-hand Sides
## 1.2 The LU Decomposition
### 1.2.1 Computation of the LU Decomposition
### 1.2.2 Use the LU Decomposition to Solve $\ $ A x = b
# 2. The PLU Decomposition
## 2.1 Required Row Exchange(s)
### 2.1.1 Example
### 2.1.2 Solving $\;$ P L U x = b
## 2.2 Variations
### 2.2.1 The PLDU Decomposition
### 2.2.2 Symmetric Matrices
# 3. Take Away
