---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '06_GE_Systems.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. The Basic Idea
## 1.1 Examples
## 1.2 Elementary Operations
# 2. Intermediate Stage: Gaussian Elimination when Writing Equations
# 3. Gaussian Elimination in Matrix Form
## 3.1 Key Insight
## 3.2 The Elimination Matrix
## 3.3 A Complete Example
# 4. Take Away
