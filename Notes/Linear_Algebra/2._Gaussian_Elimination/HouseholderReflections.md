---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'HouseholderReflections.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Reflection Onto an Elementary Basis Vector
## 1.1 The Line of Reflection
## 1.2 Householder Matrices
# 2. Householder Reflection Algorithm
## 2.1 Small Example with Rationals
## 2.2 Floating Point Example
## 2.3 Remark: this is a QR Decomposition
