---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'SymbolicVariables.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Setup
# 2. Symbolic Variables
# 3. Matrix of Symbolic and Numeric Variables and Expressions
# 4. Substitutions and Solutions
