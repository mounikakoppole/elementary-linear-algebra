
---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'GenerateProblems.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. GE Problems
## 1.1 GE and GJ Problem with Layout
## 1.2 Inverse Problem with Layout
## 2.3 PLU Problem
# 3 Normal Equation Problems
## 3.1 Solve the Normal Equation
# 4. QR Problems
# 5. Eigenproblems
