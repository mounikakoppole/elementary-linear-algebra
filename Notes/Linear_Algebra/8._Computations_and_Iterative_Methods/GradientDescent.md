---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'GradientDescent.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Find a Minimum
## 1.1 The Idea of the Gradient Descent Algorithm
## 1.2 Two Scalar Function Examples
## 1.3 What about the Cusp Function?
## 1.4 Convex Functions
# 2. Least Mean Squares Example
