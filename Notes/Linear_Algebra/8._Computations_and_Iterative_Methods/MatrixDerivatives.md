---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'MatrixDerivatives.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Introduction 
## 1.1 Einstein Summation Convention
## 1.2 Derivatives
### 1.2.1 Vector by Scalar Derivative
### 1.2.2  Scalar by Vector Derivative
### 1.2.3  Vector by Vector Derivative
### 1.2.3  Matrix by Scalar Derivative
### 1.2.4  Scalar by Matrix Derivative
### 1.2.5 The Differential of a Matrix
### 1.2.6 Example
### 1.2.7 Vector by Matrix, Matrix by Matrix, etc..
# 2. Basic Formulae
## 2.1 Sums and Products
## 2.2 The Chain Rule
### 2.2.1 Functions ${g: \mathbb{R}^n \rightarrow \mathbb{R}^m}$
### 2.2.2 Functions $g: \mathbb{R}^{m \times n} \rightarrow \mathbb{R}^{k \times l}$
## 2.3 Some Common Derivatives
## 2.4 Differentials
# 3. Examples
## 3.1. Neural Network Weight Updates
## 3.2 Least Mean Squares Objective Function
## 3.3 The Rayleigh Quotient
