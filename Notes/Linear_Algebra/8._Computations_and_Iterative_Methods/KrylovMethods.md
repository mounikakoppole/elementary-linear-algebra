
---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'KrylovMethods.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. GhatGPT Description
# 2. Definition and Basic Properties
# 3. Minimal Polynomial
## 3.1 Different Eigenvalues
## 3.2 Repeated Eigenvalues, Diagonalizable
## 3.3 Repeated Eigenvalues, non-diagonalizable
# 3.4 Other Problems
