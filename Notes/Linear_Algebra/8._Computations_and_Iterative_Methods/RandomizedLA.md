
---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'RandomizedLA.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Images are Matrices
## 1.1 Display a Matrix
## 1.2 A Color Image
# 2. Approximate the Column Space $\mathscr{C}(A)$
## 2.1 Random Vectors Tend to be Orthogonal
## 2.2 Random Samples of the Column Space
# 3. Matrix Factorizations
# 4. Randomized SVD
## 4.1 Randomized SVD
## 4.2 Randomized SVD with Power
