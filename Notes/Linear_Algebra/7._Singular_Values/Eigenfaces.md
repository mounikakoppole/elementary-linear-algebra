---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'Eigenfaces.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. A Database of Faces
## 1.1 Useful Functions
# 2 PCA of the Face Features
## 2.1 The Mean Face: an Average of All the Faces
## 2.2 The PCA Features
## 2.3 Dimensionality Reduction
# 3. Facial Recognition
