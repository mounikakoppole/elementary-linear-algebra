
---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '28_PseudoInverse.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. The Pseudoinverse
## 1.1. A Preimage of $y = A x$
## 1.2 The Pseudoinverse and the Reduced Pseudoinverse
## 1.3 **Projection Matrices**
# 2. Application to the Normal Equation
# 3. Take Away
