---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'PCA_and_SVD.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Introduction
## 1.1 Summary
## 1.2 Problem Statement
# 2. A Small 2D Example
# 1. Singular Vectors $V_r$ and Singular Values  $\Sigma$
# 2. Error Concentration Ellipses
# 3. Least Squares versus Total Least Squares
# 4. Dimensionality Reduction
# 2. A Larger Example
