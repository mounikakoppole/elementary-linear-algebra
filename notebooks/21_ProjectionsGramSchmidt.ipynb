{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "##] dev --local \"./LAcode.jl\"\n",
    "using LinearAlgebra, RowEchelon, LaTeXStrings, Plots, SymPy, LAcode\n",
    "#title( \"Some Uses of the Normal Equation\", sz=30, color=\"darkred\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align:center;\">\n",
    "<strong style=\"height:100px;color:darkred;font-size:40px;\">Some Uses of the Normal Equation</strong><br>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "html\"<iframe width=\\\"400\\\" height=\\\"200\\\" src=\\\"https://www.youtube.com/embed/kDBzpkr3-yE\\\"  frameborder=\\\"0\\\" allow=\\\"accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture\\\" allowfullscreen></iframe>\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. The Normal Equation (Reminder)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:18cm;height:5.2cm;background-color: transparent;border:1px solid black;\">\n",
    "<style type=\"text/css\">\n",
    ".tftable {font-size:12px;color:#333333;width:12cm;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}\n",
    ".tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}\n",
    ".tftable tr {background-color:#ffffff;}\n",
    ".tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}\n",
    "</style>\n",
    "\n",
    "<table class=\"tftable\" border=\"1\">\n",
    "<tr><th style=\"width:5cm;\">Equation</th><th style=\"width:5cm;\">Orthogonal Projection</th><th>Comment</th></tr>\n",
    "<tr><td>$x = \\arg\\min_x { \\lVert b - A x \\rVert }$</td><td>$b_\\parallel=A x$</td><td>Minimize Distance to $\\mathscr{C}(A)$</td></tr>\n",
    "<tr><td>$A^t A x = A^t b$</td><td>$b_\\parallel=A x$</td><td>Remove $\\mathscr{N}(A^t)$ component from $b$</td></tr>\n",
    "<tr><td>$x = \\frac{a \\cdot b}{a \\cdot a}$</td><td>$b_\\parallel=\\frac{a \\cdot b}{a \\cdot a}\\;a$</td><td>Column Vector Case $A = a$</td></tr>\n",
    "<tr><td>$x_i = \\frac{a_i \\cdot b}{a_i \\cdot a_i}$</td><td>$b_\\parallel=\\sum_i\\frac{a_i \\cdot b}{a_i \\cdot a_i}\\;a_i$</td><td>Orthogonal Vectors $a_i$ Case $A = a$</td></tr>\n",
    "<tr><td>$x_i = q_i \\cdot b$</td><td>$b_\\parallel=\\sum_i{q_i \\cdot b\\;q_i}$</td><td>Orthonormal Vectors $q_i$ Case</td></tr>\n",
    "</table>\n",
    "</div>\n",
    "<img style=\"float:center;border:1px solid black;\" width=220 src=\"Figs/OrthoProjection_into_plane.png\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Orthogonal Projection Matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.1 Theory"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To **project a vector orthogonally onto a span of vectors** $S = span\\{ a_1, a_2, \\dots a_N \\}$,<br>\n",
    "$\\quad\\quad$ we can solve the normal equation $A^t A x = A^t b$, and compute $b_\\parallel = A x$.\n",
    "\n",
    "$\\quad\\quad$ The matrix $A$ has columns $ a_1, a_2, \\dots a_N.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Remarks:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The normal equation can have an **infinite number of solutions $x$**<br>\n",
    "  $\\quad\\quad$ $x = x_p + x_h$, where $x_h$ are the homogeneous solutions,<br>\n",
    "  $\\quad\\quad$ but $\\mathbf{b_\\parallel = A (x_p + x_h ) = A x_p}$ **is unique.**\n",
    "\n",
    "* Since $\\mathscr{N}(A) = \\mathscr{N}(A^t A)$,<br>\n",
    "  $\\quad\\quad$ the normal equation has a<br>\n",
    "  $\\quad\\quad$ **unique solution iff the vectors $a_i$ are linearly independent.**\n",
    "  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* If the vectors $a_i, i=1,2, \\dots N$ are linearly independent, $\\mathbf{A^t A}$ **is invertible.**<br>\n",
    "  Otherwise, we **can remove linearly dependent vectors from the $a_i$ until we have a basis for $S$.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Solution of the Normal Equation**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assume $A$ of size $M \\times N$ has full column rank, i.e., $rank(A) = N$.<br>\n",
    "$\\quad\\quad$ so that matrix $A^t A$ is invertible.\n",
    "\n",
    "$\\quad\\quad$ Solving $ A^t A x =\\ A^t b  \\Leftrightarrow \\ x = \\ (A^t A)^{-1}\\ A^t\\ b\\;$ and computing $b_\\parallel$ and $b_\\perp$, we obtain\n",
    "$$\n",
    "   \\begin{align}\n",
    "   b_\\parallel &=&\\ \\color{blue}{ A (A^t A)^{-1} A^t } \\ b  &=&\\ \\color{blue}P\\ b \\\\\n",
    "   b_\\perp     &=&\\ b - b_\\parallel                         &=&\\ \\color{blue}{(I - P)}\\ b,\n",
    "   \\end{align}\n",
    "$$\n",
    "\n",
    "$\\quad\\quad$ where we have set $\\quad\\quad\\quad\\quad\\quad\\quad\\quad\\quad\\quad\\quad\\;\\ $ $\\color{blue}{P \\;= A (A^t A)^{-1} A^t }$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "We see that\n",
    "* **$P\\quad\\;\\;$ projects the vector $b$ orthogonally onto the column space $\\mathscr{C}(A)$**.\n",
    "* **$I - P$ projects the vector $b$ orthogonally onto the orthogonal complement $\\mathscr{C}^\\perp(A) = \\mathscr{N}(A^t)$**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Orthogonal Projection Matrices**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Theorem:** Let $\\{ a_1, a_2, \\dots a_N \\}$\n",
    "be a set of linearly independent vectors.\n",
    "\n",
    "$\\quad\\quad$ The **orthogonal projection matrix** onto the span of the vectors $a_i, i=1,2, \\dots N$ is given by\n",
    "$$\n",
    "P_\\parallel = A \\left( A^t A \\right)^{-1} A^t\n",
    "$$\n",
    "$\\quad\\quad$ The **orthogonal projection matrix** onto the orthogonal complement of the span of the vectors\n",
    "$a_i, i=1,2, \\dots N$ is given by\n",
    "$$\n",
    "P_\\perp = I - A \\left( A^t A \\right)^{-1} A^t\n",
    "$$\n",
    "\n",
    "where the $a_i$ are the columns of $A = \\left( a_1\\ a_2\\ \\dots \\ a_N \\right).$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2 Example: Orthogonal Projection onto a Span of 3 Vectors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Compute the orthogonal projection matrix onto\n",
    "> $S = span \\left\\{\\; a_1 = \\begin{pmatrix} 1 \\\\  5 \\\\  1 \\\\  2 \\end{pmatrix},\n",
    "a_2 = \\begin{pmatrix} 3 \\\\ 3 \\\\ -1 \\\\ 2 \\end{pmatrix},\n",
    "a_3 = \\begin{pmatrix} 4 \\\\ 8 \\\\ 0 \\\\ 4 \\end{pmatrix}\\;\\right\\},\\quad$<br>\n",
    "and use it to decompose\n",
    "$\\; b = \\begin{pmatrix} 15\\\\ 23\\\\ -2\\\\ 9 \\end{pmatrix}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Step 1: Obtain a Normal Equation**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Since $a_3 = a_1 + a_2$, we can remove it.** The matrix $A = \\left( a_1 \\, a_2 \\right)$ is full column rank.\n",
    ">\n",
    "> $$ A = \\begin{pmatrix}\n",
    " 1 &  3  \\\\\n",
    " 5 &  3 \\\\\n",
    " 1 & -1 \\\\\n",
    " 2 &  2 \\\\\n",
    " \\end{pmatrix} \\Rightarrow\n",
    " A^t A = \\begin{pmatrix}31 & 21 \\\\ 21 & 23 \\end{pmatrix}\\Leftrightarrow (A^t A)^{-1} = \n",
    " \\frac{1}{272} \\begin{pmatrix}23& -21 \\\\ -21 & 31 \\end{pmatrix} .\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Step 2: The Orthogonal Projection Matrices**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> The orthogonal projection matrix onto $S$ is given by\n",
    "> $$P_\\parallel =  A ( A^t A )^{-1} A^t = \\frac{1}{272}\\left(\\begin{array}{rrrr}\n",
    "  176 &  16 & -112 &  64 \\\\\n",
    "   16 & 224 &   64 &  80 \\\\\n",
    " -112 &  64 &   96 & -16 \\\\\n",
    "   64 &  80 &  -16 &  48 \\\\\n",
    "\\end{array}\\right)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> The orthogonal projection matrix onto $S^\\perp$ is given by\n",
    "> $$P_\\perp = I - P_\\parallel = \\frac{1}{272}\n",
    "\\left(\\begin{array}{rrrr}\n",
    "  96 & -16 & 112 & -64 \\\\\n",
    " -16 &  48 & -64 & -80 \\\\\n",
    " 112 & -64 & 176 &  16 \\\\\n",
    " -64 & -80 &  16 & 224\n",
    " \\end{array}\\right)\n",
    " $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Step 3: Orthogonal Projection of $b$**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> $b_\\parallel = P_\\parallel\\ b = \\begin{pmatrix} 14 \\\\ 22 \\\\ -2 \\\\ 12 \\end{pmatrix} \\quad$ and\n",
    "$\\quad b_\\perp     = P_\\perp\\ b      = \\begin{pmatrix} 1 \\\\ 1 \\\\ 0 \\\\ -3 \\end{pmatrix},$<br>\n",
    "the same result we found previously when solving the normal equation directly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.3 Example: Orthogonal Projection onto a Line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The normal equation greatly simplifies for $A = ( a )$.<br>\n",
    "We can exploit this\n",
    "to obtain simpler formulae for the projection matrices:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Example**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:43%;border:1px solid black;\">\n",
    "\n",
    "$\\quad$ **Remark:** Given a column vector $a,$ then\n",
    "$$\\begin{align}\\left( a \\cdot b \\right) &= a^t b,\\quad\\text{so that} \\\\\n",
    "a\\ (a \\cdot b) &= a a^t \\ b.\\end{align}$$\n",
    "\n",
    "$$\\begin{align}\n",
    "a \\cdot a x = a \\cdot b&\\; \\Leftrightarrow \\; & x & & = \\frac{1}{ \\lVert a \\rVert^2 } a \\cdot b \\\\\n",
    "                       &\\; \\Rightarrow \\; & b_\\parallel & = A x & = \\color{red}{\\frac{1}{ \\lVert a \\rVert^2 } a a^t}\\ b  \\\\\n",
    "                       &                  &                   & = \\color{red}{P_\\parallel}\\ b\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "</div>\n",
    "<img src=\"Figs/NormalProjOntoLine.svg\" width=280 style=\"float:left;\">\n",
    "<div style=\"float:right;width:6cm;height:4.8cm;border:1px solid black;\">\n",
    "\n",
    "$\\quad$ **Alternate derivation**\n",
    "$$\\begin{align}\n",
    "P_\\parallel =&\\ a (a^t a)^{-1} a^t \\\\\n",
    "            =&\\ a \\left( \\frac{1}{a \\cdot a} I_{1 \\times 1}\\right) a^t \\\\\n",
    "            =&\\ \\frac{1}{a \\cdot a} a a^t.\n",
    "\\end{align}$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Let\n",
    "> $$\\quad a = \\begin{pmatrix} 3 \\\\ 4 \\\\ 0 \\end{pmatrix}, \\quad b =  \\begin{pmatrix} 0 \\\\ 5 \\\\ 2 \\end{pmatrix}.$$\n",
    ">\n",
    "> $$\n",
    "P_\\parallel =\\color{red}{ \\frac{1}{25} \\begin{pmatrix} 3 \\\\ 4 \\\\ 0 \\end{pmatrix} \\begin{pmatrix} 3 & 4 & 0 \\end{pmatrix}}\n",
    "            =  \\frac{1}{25} \\begin{pmatrix} 9 & 12 & 0 \\\\ 12 & 16 & 0 \\\\ 0 & 0 & 0 \\end{pmatrix}, \\quad\\quad\n",
    "P_\\perp = I - P_\\parallel = \\frac{1}{25} \\begin{pmatrix} 16 & -12 & 0 \\\\ -12 & 9 & 0 \\\\ 0 & 0 & 25 \\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> $$b_\\parallel = P_\\parallel b = \\frac{1}{5} \\begin{pmatrix} 12 \\\\ 16\\\\ 0 \\end{pmatrix},\\quad\\quad\n",
    "b_\\perp = P_\\perp b = \\frac{1}{5} \\begin{pmatrix} -12 \\\\ 9 \\\\ 2 \\end{pmatrix}\n",
    "$$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.4 Projection onto an Orthogonal Basis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For mutually orthogonal non-zero vectors $a_i, i=1,\\dots N$, the normal equations reduce to\n",
    "$$\n",
    "A^t A x = A^t b \\Leftrightarrow x = (A^tA)^{-1} A^t b,\n",
    "$$\n",
    "where\n",
    "$$\n",
    "(A^t A)^{-1} = \\begin{pmatrix} \\frac{1}{a_1 \\cdot a_1} & 0                   & \\dots & 0 \\\\\n",
    "                    0                   & \\frac{1}{a_2 \\cdot a_2} & \\dots & 0 \\\\\n",
    "                    \\                   &     \\               &  \\    & 0 \\\\\n",
    "                    0                   & 0                   & \\dots & \\frac{1}{a_N \\cdot a_N} \\end{pmatrix} \\quad\n",
    "\\Rightarrow \\color{red}{ P_\\parallel = A (A^t A)^{-1} A^t =\\sum_{i=1}^N {\\frac{1}{ a_i \\cdot a_i } a_i a_i^t} }.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Remark:** For mutually orthogonal non-zero vectors $a_i, i=1,\\dots N$, the projection matrix $P_\\parallel$ reduces to\n",
    "$$\n",
    "P_\\parallel = P_{1 \\parallel} +  P_{2 \\parallel} + \\dots P_{N \\parallel}, \\quad\\text{ where }\\quad\n",
    "P_{i \\parallel} = \\frac{1}{a_i \\cdot a_i} a_i a_i^t.\n",
    "$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Example**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> $ a_1 = \\begin{pmatrix} -4\\\\ -8\\\\  1 \\end{pmatrix}, \\;\n",
    "  a_2 = \\begin{pmatrix}  7\\\\ -4\\\\ -4 \\end{pmatrix}, \\;\n",
    "  a_3 = \\begin{pmatrix}  4\\\\ -1\\\\  8 \\end{pmatrix} \\quad \\Rightarrow \\quad\n",
    " \\left\\{ \\begin{align}  a_1 \\cdot a_1 =  81, &\\quad a_2 \\cdot a_2 =  81, \\quad a_3 \\cdot a_3 = 81, \\\\\n",
    "                        a_1 \\cdot a_2 = \\;0, &\\quad a_1 \\cdot a_3 = \\;0, \\quad\\; a_2 \\cdot a_3 = \\;0. \\end{align} \\right.\n",
    "$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> $$\n",
    "P_{1 \\parallel} = \\frac{1}{81} \\begin{pmatrix}  16 & -28 & -16\\\\ -28 &  49 &  28 \\\\ -16 &  28 &  16\\end{pmatrix}, \\quad\n",
    "P_{2 \\parallel} = \\frac{1}{81} \\begin{pmatrix}   64 & 32 & 8 \\\\  32 & 16 & 4\\\\  8 & 4 & 1\\\\ \\end{pmatrix}, \\quad\n",
    "P_{3 \\parallel} = \\frac{1}{81} \\begin{pmatrix}   1 & -4 & 8 \\\\  -4 & 16 & -32\\\\  8 & -32 & 64\\\\ \\end{pmatrix}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> The orthogonal projection matrix onto the $span \\{ a_1, a_2 \\}$ is therefore given by\n",
    "> $$\n",
    "P_{1 2 \\parallel} = P_{1 \\parallel} + P_{2 \\parallel}=\n",
    "\\frac{1}{81} \\begin{pmatrix} 80 & 4 & -8 \\\\\n",
    "  4 & 65 & 32 \\\\\n",
    " -8 & 32 & 17 \\end{pmatrix}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Orthonormal Bases"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.1 Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Let $a_1 = \\frac{1}{5} \\begin{pmatrix}  3 \\\\ 4 \\\\ 0\\end{pmatrix},\n",
    "     a_2 = \\frac{1}{5} \\begin{pmatrix}  -4 \\\\ 3 \\\\ 0\\end{pmatrix},\n",
    "     a_3 =             \\begin{pmatrix}  0 \\\\ 0 \\\\ 1\\end{pmatrix}.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Verify that these vectors form an orthonormal set:<br>\n",
    "> $A = \\left( a_1 \\ a_2 \\ a_3 \\right) \\ = \\frac{1}{5} \\begin{pmatrix} 3 & -4 & 0 \\\\ 4 & 3 & 0 \\\\ 0 & 0 & 5 \\end{pmatrix} \\Rightarrow A^t A = \\begin{pmatrix} 1 & 0 & 0 \\\\ 0 & 1 & 0 \\\\ 0 & 0 & 1 \\end{pmatrix}.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ">\n",
    "> * **Since $A$ is square, we see that $A^{-1} = A^t$**\n",
    "> * The vectors $a_1, a_2, a_3$ form an orthonormal basis for $\\mathbb{R}^3$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.2 Orthogonal Matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:50%;height:2.5cm;padding:5px;background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:** A matrix $A$ is **orthogonal** iff $A^t = A^{-1}.$\n",
    "    \n",
    "**Definition:** A matrix $A$ is **unitary** iff $A^H = A^{-1}.$<br>\n",
    "$\\quad\\quad$ (The **hermitian transpose** $A^H$ is the complex conjugate of $A^t.$)\n",
    "\n",
    "</div>\n",
    "<div style=\"float:right;margin:10px;width:40%height:2.5cm;\">\n",
    "\n",
    "$\\;\\;$ **Remark:** Therefore the matrix $A$ is square,<br>$\\quad\\quad$ and the columns of $A$ are mutually orthogonal and unit length.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Important Properties of Orthogonal and Unitary Matrices**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ">Applying orthogonal and unitary matrices to a vector **does not change the length of a vector,**<br>\n",
    "and does not change the angle between vectors:<br><br>\n",
    "**Computations involving orthogonal vectors will not overflow!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Theorem:** Let $Q$ be an orthogonal (or a unitary) matrix, and let $x$ and $y$ be vectors of consistent length\n",
    "* $\\lVert Q x \\rVert =  \\lVert x \\rVert, \\quad\\quad\\quad\\;$ **lengths are conserved**\n",
    "*  $(Q x) \\cdot (Q y) = x \\cdot y,\\quad$ **angles are conserved**\n",
    "*  $Q x \\perp Q y \\Leftrightarrow x \\perp y, \\quad$ in particular, **orthogonal angles are conserved.**\n",
    "    </div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Example**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Consider $\\quad$ $Q = \\frac{1}{2}\\begin{pmatrix} 1 & 1 \\\\ 1 & 1 \\\\ 1 & -1 \\\\ 1& -1 \\end{pmatrix}$, and $\\quad$ $x = \\begin{pmatrix} 1 \\\\ 2 \\end{pmatrix}, y= \\begin{pmatrix} -2\\\\1 \\end{pmatrix}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:50%;\">\n",
    "\n",
    "> $Q^t Q = \\begin{pmatrix} 1 & 0 \\\\ 0 & 1 \\end{pmatrix},$ $\\quad$ so $Q$ has orthonormal columns.\n",
    ">\n",
    "> $Q$ is not square: it does not have an inverse.\n",
    ">\n",
    "> $Q Q^t = \\frac{1}{2}\\left(\\begin{array}{rrrr}\n",
    "1 & 1 & 0 & 0 \\\\\n",
    "1 & 1 & 0 & 0 \\\\\n",
    "0 & 0 & 1 & 1 \\\\\n",
    "0 & 0 & 1 & 1\n",
    "        \\end{array}\\right) .\n",
    "$\n",
    "</div><div style=\"float:left;width:40%\">\n",
    "\n",
    "> $x \\cdot y = 0 \\Rightarrow x \\perp y$\n",
    ">\n",
    "> $\\lVert x \\rVert = \\sqrt{5},$ <br>\n",
    "> $Q x = \\frac{1}{2} \\begin{pmatrix} 3 \\\\ 3 \\\\ -1 \\\\ -1 \\end{pmatrix} \\Rightarrow \\lVert Q x \\rVert = \\sqrt{5}$\n",
    "\n",
    "> $(Q x) \\cdot (Q y) = \n",
    " \\frac{1}{2} \\begin{pmatrix} 3 \\\\ 3 \\\\ -1 \\\\ -1 \\end{pmatrix} \\cdot\n",
    " \\frac{-1}{2} \\begin{pmatrix} 1 \\\\ 1 \\\\ 3 \\\\ 3 \\end{pmatrix} = 0$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> If we augment $Q$ with an orthonormal basis of $\\mathscr{N}(Q^t)$<br>\n",
    "> $ \\tilde{Q} = \\frac{1}{2}\\begin{pmatrix} 1 &  1 &  1 &  0 \\\\\n",
    "                                          1 &  1 & -1 &  0 \\\\\n",
    "                                          1 & -1 &  0 &  1 \\\\\n",
    "                                          1 & -1 &  0 & -1 \\end{pmatrix}, \\quad\n",
    "$ then $\\quad \\tilde{Q} \\tilde{Q}^t = I, \\tilde{Q}^t \\tilde{Q} = I, \\quad$ and therefore $Q^{-1} = Q^t.$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.3 (Extra Material) A Naive Construction Method for Orthogonal Matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "Can we find sets of mutually orthogonal vectors?\n",
    "\n",
    "One way to do this is to use the fundamental theorem of linear algebra:<br>\n",
    "The span of the columns of a matrix $A$ is orthogonal to the null space $\\mathscr{N}(A^t)$\n",
    "\n",
    "We can proceed one vector at a time:\n",
    "* starting with a single vector $v_1$, set $A=(v_1)$ and use GE on $(A I)$ to find a vector $v_2 \\in \\mathscr{N}(A^t)$\n",
    "* set $A=(v_1\\; v_2)$ and use GE on $(A I)$ to find a vector $v_3 \\in \\mathscr{N}(A^t)$\n",
    "* set $A=(v_1\\; v_2\\; v_3)$ and use GE $\\dots$\n",
    "We will have constructed a set of mutually orthogonal vectors\n",
    "\n",
    "**Remark:** this method works well in exact arithmetic. It is not useful for actual computations (**see Gramm-Schmidt for a better method**)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "title(\"An Example of Mutually Orthogonal Vectors\")\n",
    "v  = [1//1; 2; -1; 1]\n",
    "A  = [v zeros(Int64,4,3) Matrix(1I,4,4)]\n",
    "E1 = [1 0 0 0; -2 1 0 0; 1 0 1 0; -1 0 0 1] ; A1 = E1*A\n",
    "title(\"first vector\", sz=10)\n",
    "ge_layout( A, [E1 A1], [], col_divs=4)\n",
    "\n",
    "A[:,2]=A1[2,5:end]; A1=E1*A\n",
    "E2 = [1//1 0 0 0; 0 1 0 0; 0 2//5 1 0; 0 -2//5 0 1] ; A2 = E2*A1\n",
    "title(\"add in second vector, complete GE for this vector and the right hand side (nothing else changes)\", sz=10)\n",
    "ge_layout( A, [E1 A1; E2 A2], [], col_divs=4)\n",
    "\n",
    "A[:,3]=A2[3,5:end]; A1=E1*A; A2=E2*A1\n",
    "E3 = [1//1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 1//6 1] ; A3 = E3*A2\n",
    "A[:,4]=A3[4,5:end]\n",
    "\n",
    "title(\"add in the third vector, complete GE for this vector and the right hand side (nothing else changes)\", sz=10)\n",
    "ge_layout( A, [E1 A1; E2 A2; E3 A3], [], col_divs=4)\n",
    "title(\"add in the last vector, no more computation required\", sz=10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "title(\"Check that the columns of the resulting matrix are orthogonal\", sz=10)\n",
    "A=A[:,1:4]\n",
    "ge_layout(A'A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "**Definition:** An **orthogonal matrix** is a square matrices $Q$ with *orthonormal* columns: $Q^t Q = I$.<br>\n",
    "                A **unitary matrix** is a square matrix $Q$ such that $Q^H Q = I.$ ($Q^H$ is the conjugate transpose of $Q$, aka the hermitian transpose.)\n",
    "\n",
    "Since $Q$ is square and $Q^t Q = I$, we have $Q^{-1} = Q^t$\n",
    "\n",
    "**Remark:** the matrix must be square. Otherwise $Q^t$ is a left inverse only!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1.3 Two Important Examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "* A useful example of orthogonal matrices is the **Haar matrix** (wavelets). See [wikipedia](https://en.wikipedia.org/wiki/Haar_wavelet)\n",
    "* A useful example of a unitary matrix is the **Discrete Fourier Transform (DFT) matrix**. See [wikipedia](https://en.wikipedia.org/wiki/DFT_matrix)\n",
    "    </div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.4 Gramm-Schmidt Orthogonalization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **The Problem**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we saw, **orthogonal basis vectors are really nice:**\n",
    "just think $\\{ i, j, k \\}$ in $\\mathbb{R}^3.$\n",
    "\n",
    "> Given a set of basis vectors for a hyperplane in $\\mathbb{R}^N$,<br>\n",
    "we would like to construct an **orthogonal basis** for this hyperplane.<br>\n",
    "Better yet, we would like to construct an **orthonormal basis** for this hyperplane."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **The Idea**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:12cm;\">\n",
    "\n",
    "**Idea:** Decompose **each vector in turn** by projecting it onto the span<br>$\\quad\\quad$ of the previous set of vectors,<br>\n",
    "$\\quad\\quad$ **keeping only the orthogonal component.**\n",
    "    \n",
    "---\n",
    "* $\\color{blue}{w_1 = v_1},$<br> so $span\\{w_1\\} = span\\{v_1\\},\\quad\\quad\\quad\\quad\\quad\\quad$ i.e., the blue line.\n",
    "    \n",
    "* $\\color{magenta}{w_2 = v_2 - Proj^\\perp_{span\\{ v_1 \\}}\\; v_2},$<br>\n",
    "    so $span\\{ v_1, v_2 \\} = span\\{w_1, w_2\\},\\quad\\quad\\quad\\;$ i.e., the plane.\n",
    "    \n",
    "* $\\color{brown}{w_3 = v_3 - Proj^\\perp_{span\\{ v_1, v_2 \\}}\\; v_3},$<br>\n",
    "    so $span\\{ v_1, v_2, v_3 \\} = span\\{w_1, w_2, w_3\\},\\quad$ i.e., 3 space.\n",
    "\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:1cm;\">\n",
    "<img src=\"Figs/GramSchmidt.png\" style=\"float:center;border:1px solid black;\" width=300>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Refinement**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:60%;background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**The Gram-Schmidt Procedure**\n",
    "$$\n",
    "\\begin{align}\n",
    "w_1 & = \\color{red}{v_1} && \\\\\n",
    "w_2 & = \\color{red}{v_2} - Proj^\\perp_{ span\\{ w_1      \\} } \\; \\color{red}{v_2} &=&\\ \\color{red}{v_2} - \\frac{\\color{red}{v_2} \\cdot w_1}{w_1 \\cdot w_1} w_1\\\\ \n",
    "w_3 & = \\color{red}{v_3} - Proj^\\perp_{ span\\{ w_1, w_2 \\} } \\; \\color{red}{v_3} & =&\\ \\color{red}{v_3} - \\frac{\\color{red}{v_3} \\cdot w_1}{w_1 \\cdot w_1} w_1\n",
    "                                                                 - \\frac{\\color{red}{v_3} \\cdot w_2}{w_2 \\cdot w_2} w_2 \\\\   \n",
    " \\dots& \\dots\\dots\\dots\\dots\\dots\\dots & &\\dots\\dots\\dots\\dots\\dots\\dots\\dots \\\\\n",
    "w_k & = \\color{red}{v_k} - Proj^\\perp_{ span\\{ w_1, w_2, \\dots w_{k-1} \\} } \\; \\color{red}{v_k} &&\n",
    "\\end{align}\n",
    "$$\n",
    "</div><div style=\"float:right;width:35%;\">\n",
    "\n",
    "**Use the $w_i$ vectors to compute the projections,**<br>\n",
    "$\\quad\\quad$ rather than the $v_i$ vectors.<br><br>$\\quad\\quad$ **They are orthogonal!**\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We complete the procedure by making the vectors **unit length:**<br><br>\n",
    "<div style=\"float:left;width:14cm;background-color:#F2F5A9;color:black;\">\n",
    "<br>\n",
    "$\\;\\; q_i = \\frac{1}{\\lVert w_i \\rVert } w_i, \\; 1=1,2, \\dots k. $\n",
    "<br><br>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Example**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider $v_1 = \\begin{pmatrix}1 \\\\ 0 \\\\ 1 \\\\ 1 \\end{pmatrix},\n",
    "v_2 =  \\begin{pmatrix}0 \\\\ 1 \\\\ 0 \\\\ 1 \\end{pmatrix},\\; \\text{ and }\\;\n",
    "v_3 = \\begin{pmatrix} -1 \\\\ 1 \\\\ -1 \\\\ 1 \\end{pmatrix}.\n",
    "$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### $\\mathbf{w_1, q_1}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$w_1 = v_1 = \\begin{pmatrix} 1\\\\ 0 \\\\ 1 \\\\ 1 \\end{pmatrix},\\quad w_1\\cdot w_1 = 3, \\quad q_1 = \\frac{1}{\\sqrt{3}}\\begin{pmatrix} 1 \\\\ 0 \\\\ 1 \\\\ 1 \\end{pmatrix}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### $\\mathbf{w_2, q_2}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$w_2 = v_2 - \\frac{v_2\\cdot w_1}{w_1 \\cdot w_1}\\ w_1 \\;\n",
    "     = \\; \\begin{pmatrix}0 \\\\ 1 \\\\ 0 \\\\ 1 \\end{pmatrix}\n",
    "        - \\frac{1}{3} \\begin{pmatrix} 1 \\\\ 0 \\\\ 1 \\\\ 1 \\end{pmatrix}\n",
    "     = \\quad \\frac{1}{3} \\begin{pmatrix} -1\\\\ 3\\\\ -1 \\\\ 2 \\end{pmatrix}, \\quad\\quad w_2 \\cdot w_2 = \\frac{5}{3},\n",
    "     \\quad q_2 = \\frac{1}{\\sqrt{15}} \\begin{pmatrix} -1 \\\\ 3 \\\\ -1 \\\\ 2 \\end{pmatrix}\n",
    "$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### $\\mathbf{w_3,q_3}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\begin{align}\n",
    "&w_3 &=&\\; v_3 - \\frac{v_3\\cdot w_1}{w_1 \\cdot w_1} \\ w_1 - \\frac{v_3\\cdot w_2}{w_2 \\cdot w_2} \\ w_2 \\\\\n",
    "&    &=&\\;  \\begin{pmatrix}-1 \\\\ 1 \\\\ -1 \\\\ 1 \\end{pmatrix}\n",
    "        - \\frac{-1}{3} \\begin{pmatrix} 1 \\\\ 0 \\\\ 1 \\\\ 1 \\end{pmatrix}\n",
    "        - \\frac{7}{5} \\begin{pmatrix} -1 \\\\ -3 \\\\ -1 \\\\ 2 \\end{pmatrix}\n",
    "     = \\quad \\frac{1}{5} \\begin{pmatrix} -1\\\\ -2\\\\ -1 \\\\ 2 \\end{pmatrix}, \\quad\\quad w_3 \\cdot w_3 =\\; \\frac{2}{5}, \\quad\\quad q_3 =\\; \\frac{1}{\\sqrt{10}} \\begin{pmatrix} -1 \\\\ -2 \\\\ -1 \\\\ 2 \\end{pmatrix}\n",
    "\\end{align}\n",
    "$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. Take Away"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4.1 Projection Matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:18cm;height:5.2cm;background-color: transparent;border:1px solid black;\">\n",
    "<style type=\"text/css\">\n",
    ".tftable {font-size:12px;color:#333333;width:12cm;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}\n",
    ".tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}\n",
    ".tftable tr {background-color:#ffffff;}\n",
    ".tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}\n",
    "</style>\n",
    "\n",
    "<table class=\"tftable\" border=\"1\">\n",
    "<tr><th style=\"width:5cm;\">Orthogonal Projection Matrix</th><th>Comment</th></tr>\n",
    "<tr><td>$P_\\parallel = A ( A^t A )^{-1} A^t$</td><td>Assuming $A^t A$ is invertible</td></tr>\n",
    "<tr><td>$P_\\parallel = \\frac{1}{a \\cdot a} a a^t$</td><td>Column Vector Case: $A = a$</td></tr>\n",
    "<tr><td>$P_{\\parallel} = \\sum_{i}{\\frac{1}{a_i \\cdot a_i} a_i a^t_i}$</td><td>Orthogonal Vectors $a_i$ Case: $A = ( a_1\\ a_2 \\dots )$ </td></tr>\n",
    "<tr><td>$P_{\\parallel} = \\sum_{i}{q_i q^t_i}$<td>Orthonormal Vectors $q_i$ Case: $A = ( q_1\\ q_2 \\dots )$</td></tr>\n",
    "</table>\n",
    "</div>\n",
    "<img style=\"float:center;border:1px solid black;\" width=220 src=\"Figs/OrthoProjection_into_plane.png\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4.2 The Gram Schmidt Procedure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:80%;background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**The Gram-Schmidt Procedure**\n",
    "$$\n",
    "\\begin{align}\n",
    "w_1 & = \\color{red}{v_1} && & \\quad q_1 = \\frac{1}{\\lVert w_1 \\rVert} w_1 \\\\\n",
    "w_2 & = \\color{red}{v_2} - Proj^\\perp_{ span\\{ w_1      \\} } \\; \\color{red}{v_2} &=&\\ \\color{red}{v_2} - \\frac{\\color{red}{v_2} \\cdot w_1}{w_1 \\cdot w_1} w_1& \\quad q_2 = \\frac{1}{\\lVert w_2 \\rVert} w_2 \\\\ \n",
    "w_3 & = \\color{red}{v_3} - Proj^\\perp_{ span\\{ w_1, w_2 \\} } \\; \\color{red}{v_3} & =&\\ \\color{red}{v_3} - \\frac{\\color{red}{v_3} \\cdot w_1}{w_1 \\cdot w_1} w_1\n",
    "                                                                 - \\frac{\\color{red}{v_3} \\cdot w_2}{w_2 \\cdot w_2} w_2 & \\quad q_3 = \\frac{1}{\\lVert w_3 \\rVert} w_3 \\\\   \n",
    " \\dots& \\dots\\dots\\dots\\dots\\dots\\dots & &\\dots\\dots\\dots\\dots\\dots\\dots\\dots & \\\\\n",
    "w_k & = \\color{red}{v_k} - Proj^\\perp_{ span\\{ w_1, w_2, \\dots w_{k-1} \\} } \\; \\color{red}{v_k} &&\n",
    "& \\quad q_k = \\frac{1}{\\lVert w_k \\rVert} w_k \n",
    "\\end{align}\n",
    "$$\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.3",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
