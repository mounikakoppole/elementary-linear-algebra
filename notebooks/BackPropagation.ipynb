{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "5f80b083-af89-4dfe-b070-c63b6b42e6fe",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align: center;\"><strong style=\"height:60px;color:darkred;font-size:40px;\">The Backpropagation Algorithm</strong></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c16c7233-f73d-47a4-879e-6f6bb6413337",
   "metadata": {},
   "source": [
    "The backpropagation algorithm uses the chain rule to update a set of derivatives.<br>\n",
    "$\\qquad$ It is easy to explain using a simple example."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64643911-fb9c-4f64-afe5-3db963ac75b6",
   "metadata": {},
   "source": [
    "# Example"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4524a92-a35c-4573-a1d9-61de169a0f3c",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:55%;\">\n",
    "Consider $f(x,y) = 5 x^2 -3 y, g(x,y)= 2 x + y -5$ and $h(x,y) = x y$.<br><br>\n",
    "$\\qquad$ Let's compute $\\frac{\\partial}{\\partial x} \\left. { h(f(x,y), g(x,y) } \\right|_{x=1,y=3}$<br><br>\n",
    "\n",
    "Using the chain rule, we obtain:\n",
    "\n",
    "$\\qquad \\begin{align} \\frac{\\partial}{\\partial x} { g(x,y) f(x,y) } = \\;\n",
    "      & \\left.\\frac{\\partial h(f,g)}{\\partial f}\\right|_{f=f(x,y), g=g(x,y)} \\frac{\\partial f(x,y)}{\\partial x} \\\\\n",
    "    + & \\left.\\frac{\\partial h(f,g)}{\\partial g}\\right|_{f=f(x,y), g=g(x,y)} \\frac{\\partial g(x,y)}{\\partial x}\n",
    "    \\end{align}\n",
    "$\n",
    "<br>\n",
    "\n",
    "where we have introduced variables $f$ and $g$ that will be evaluated as<br> $\\qquad f = f(x,y)$ and $g = g(x,y)$.<br><br>\n",
    "\n",
    "The graph representing the operations is shown on the right:<br>\n",
    "\n",
    "Since we are interested in specific values $x=1$ and $y=3$,<br>\n",
    "$\\qquad$ each function and each derivative<br>$\\qquad$ need to be computed for these values.\n",
    "</div><div style=\"float:left;width:45%;\">\n",
    "    <img src=\"Figs/backpropagation.svg\" width=\"450\">\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14cbafc7-be34-4efe-b136-9776e6f9e29c",
   "metadata": {},
   "source": [
    "The computation proceeds in two phases:\n",
    "* phase 1: start at the bottom, and substitute the values for $x$ and $y$ in each of the functions as we work up toward $h$.<br>\n",
    "$\\qquad \\left.\n",
    "\\left. \\begin{align}\n",
    "x=1, y=3 \\\\\n",
    "f=5x^2-3y, g=2x+y-5\n",
    "\\end{align}\\right\\} \\Rightarrow f = -4, g= 0 \\right\\} \\Rightarrow h = f g = 0$\n",
    "\n",
    "* phase 2: start at the top, and evaluate each of the derivatives as we move down to the variables $x$ and $y$<br>\n",
    "using the values calculated in phase 1.\n",
    "\n",
    "$ \\qquad \\begin{align}\n",
    "& \\frac{\\partial h(f,g)}{\\partial f} = g = 0,    & \\frac{\\partial h(f,g)}{\\partial g} =& f = -4 & \\\\\n",
    "& \\frac{\\partial f(x,y)}{\\partial x} = 10 x =10, & \\frac{\\partial g(x,y)}{\\partial x} =& \\ 2  &\\\\\n",
    "& \\frac{\\partial f(x,y)}{\\partial y} = -3,       & \\frac{\\partial g(x,y)}{\\partial y} =& \\ 1,  & \\frac{\\partial h}{\\partial x} = -8, \\;\\;\\frac{\\partial h}{\\partial y} = -4\\\\\n",
    "\\end{align}\n",
    "$\n",
    "\n",
    "**Remark:** this computes each of the partial derivatives."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3c03d4e-ab8f-41ed-8186-80bb3cee4a65",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:45%;text-align:center;\">\n",
    "<strong>Backpropagation Phase 1</strong>\n",
    "\n",
    "<br><br>\n",
    "<img src=\"Figs/backpropagation_phase_1.svg\" width=\"450\">\n",
    "</div>\n",
    "<div style=\"float:right;width:45%;text-align:center;\">\n",
    "<strong>Backpropagation Phase 2</strong>\n",
    "<br><br>\n",
    "<img src=\"Figs/backpropagation_phase_2.svg\" width=\"450\">\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f6dff1d3-8089-4aa1-a389-9d68ae106db9",
   "metadata": {},
   "source": [
    "**Remark:** If we do not require $\\frac{\\partial h}{\\partial y}$, we do not need to compute $\\frac{\\partial f}{\\partial y}$ and $\\frac{\\partial g}{\\partial y}.$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6016fb3-531e-4baf-bf52-780f5397f4ba",
   "metadata": {},
   "source": [
    "The **backpropagation** terminology is due to neural networks:<br>\n",
    "$\\qquad$ the variables $x,y$ are values at the input layer.<br>\n",
    "$\\qquad$ As we move upward in the dependency graph, we move toward the output layer.\n",
    "\n",
    "$\\qquad$ Phase 2 computes the partial derivatives at the output layer,<br>\n",
    "$\\qquad$ then propagates these values back toward the input layer<br>\n",
    "$\\qquad$ by making use of the chain rule."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e963d1a-39e0-4351-984d-049505118385",
   "metadata": {},
   "source": [
    "# Forward Versus Backpropagation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b490213d-ca5f-44e7-b0a8-bc3a5886dc02",
   "metadata": {},
   "source": [
    "Let us expand the example by one layer, and compute the partial derivatives by forward propagation as well as backpropagation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5fc9a9c8-f034-4ee3-b23d-a47ebf1f83ac",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:45%;text-align:center;\">\n",
    "<strong>Forward Propagation</strong>\n",
    "<img src=\"Figs/backpropagation_forward.svg\"  width=\"450\">\n",
    "\n",
    "Using forward propagation, we start at the input layer and compute<br>\n",
    "$\\qquad$ the partial derivatives along each path<br>\n",
    "$\\qquad$ **with respect to the input variable,** e.g.,<br><br>\n",
    "$\\qquad\\qquad \\frac{\\partial f}{\\partial \\color{red}{a}} = \\frac{\\partial f}{\\partial x}\\frac{\\partial x}{\\partial a} +  \\frac{\\partial f}{\\partial y} \\frac{\\partial y}{\\partial a}$\n",
    "\n",
    "$\\qquad$ We obtain the partial derivatives of each node<br>$\\qquad\\qquad$ with respect to the input variable $a$.\n",
    "</div>\n",
    "<div style=\"float:right;width:45%;text-align:center;\">\n",
    "<strong>Backward Propagation</strong>\n",
    "<img src=\"Figs/backpropagation_backward.svg\"  width=\"450\">\n",
    "\n",
    "Using backward propagation, we start at the output layer and compute<br>\n",
    "$\\qquad$ the partial derivatives of the output variable along each path<br>\n",
    "$\\qquad$ **with respect to the the current node,** e.g.,<br><br>\n",
    "$\\qquad\\qquad \\frac{\\partial \\color{red}{h}}{\\partial x} = \\frac{\\partial h}{\\partial f}\\frac{\\partial f}{\\partial x} +  \\frac{\\partial h}{\\partial g} \\frac{\\partial g}{\\partial x}$\n",
    "\n",
    "$\\qquad$We obtain the partial derivatives of the output $h$<br>$\\qquad\\qquad$ with respect to each node.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe27a847-baaa-4802-9a34-9ca3ae68461e",
   "metadata": {},
   "source": [
    "In machine learning, we are concerned with the partial derivatives of the outputs with respect to the inputs:\n",
    "* backsubstitution computes all of them in a single pass, e.g., $\\frac{\\partial h}{\\partial x}$ and  $\\frac{\\partial h}{\\partial y}$\n",
    "* forward substitution requires a pass for each input variable."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42a0c664-018d-4b53-ac8c-2d404ab8b1a8",
   "metadata": {},
   "source": [
    "____\n",
    "A good explanation is given by [Chris Olah](https://colah.github.io/posts/2015-08-Backprop/)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
